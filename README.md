# Waypoint

Central http redirection manager. This is essentially [yourls](https://github.com/YOURLS/YOURLS)
but with the ability to specify response codes (`301`/`302`).

Uses Laravel.

## Container usage

```sh
docker run -d -v /path/to/config.env:/var/www/html/.env:ro -p 3000:80 registry.gitlab.com/koel/waypoint/server
```

## Dev Setup

Set up composer and php, then

Get dependencies:
```sh
composer install
```

Set up app config by copying `.env.example` to `.env`.

Set up db with
```sh
php artisan migrate
```

Go to `/` in your browser and login.

## Development

```sh
php artisan serve
```
