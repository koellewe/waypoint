<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Models\Redir;
use Illuminate\Support\Str;

require_once __DIR__ . '/../utils/utils.php';

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });


Route::post('/new', function(Request $req){
  $RESERVED_ROUTES = ['admin', 'api'];

  if (legit($req)){
    if (Redir::where('slug', $req->input('slug'))->first()){
      return response('Slug already registered', 409);
    }else{
      foreach($RESERVED_ROUTES as $resroute){
        if (Str::startsWith($req->input('slug'), $resroute)){
          return response('Slug reserved.', 409);
        }
      }
      $redir = new Redir;
      $redir->slug = $req->input('slug');
      $redir->dest = $req->input('dest');
      $redir->code = $req->input('code');
      $redir->save();
      return redirect('/admin/success');
    }
  }else{
    return response('Bad auth', 403);
  }
});

Route::post('/edit/{id}', function($id, Request $req){
  if (legit($req)){
    $redir = Redir::find($id);
    if ($redir){
      $redir->slug = $req->input('slug');
      $redir->dest = $req->input('dest');
      $redir->code = $req->input('code');
      $redir->save();
      return redirect('/admin/success');
    }else{
      return response('Bad id', 404);
    }
  }else{
    return response('Bad auth', 403);
  }
});

Route::post('/delete/{id}', function($id, Request $req){
  if (legit($req)){
    if (Redir::find($id)){
      Redir::destroy($id);
      return redirect('/admin/success');
    }else{
      return response('Bad id', 404);
    }
  }else{
    return response('Bad auth', 403);
  }
});
