<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Waypoint Admin Login</title>

    </head>
    <body>
      <h1>Waypoint Admin Login</h1>

      <form action="/admin" method="post">
        @csrf 
        <label for="pwd">Password</label>
        <input type="password" id="pwd" name="pwd"/>
        <button type="submit" >Submit</button>
      </form>

    </body>

</html>
