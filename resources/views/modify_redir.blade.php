<html>
<head>
  <title>Waypoint: New redirect</title>
</head>
<body>
  <h2>Create new redirect</h2>
  <a href="/admin">Back</a>

  <form action="/api/{{ $mode === 'NEW' ? 'new' : 'edit/'.$redir->id }}" method="post">
    @csrf
    <label for="slug">{{ env('APP_URL') }}/</label>
    <input type="text" name="slug" id="slug" value="{{$mode === 'EDIT' ? $redir->slug : ''}}"/>
    <br/>

    <label for="dest">Destination: </label>
    <input type="text" name="dest" id="dest" value="{{$mode === 'EDIT' ? $redir->dest : ''}}"/>
    <br/>

    <label for="code">Status code: </label>
    <select name="code" id="code">
      @foreach(['301', '302'] as $code)
        @if($mode === 'EDIT' && $redir->code == $code)
          <option value="{{$code}}" selected>{{$code}}</option>
        @else
          <option value="{{$code}}">{{$code}}</option>
        @endif
      @endforeach
    </select>
    <br/>

    <button type="submit">Submit</button>
  </form>

</body>

</html>
