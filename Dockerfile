FROM php:8-apache

RUN docker-php-ext-install mysqli pdo pdo_mysql

WORKDIR /var/www/html
COPY ./ ./
RUN chown -R www-data:www-data ./
COPY apache/waypoint.conf /etc/apache2/sites-available/

RUN a2dissite 000-default
RUN a2ensite waypoint
RUN a2enmod rewrite

# Ready to host :)
